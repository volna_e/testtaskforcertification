﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace TestTaskForCertification.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DrillBlock",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DrillBlock", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Hole",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: false),
                    DrillBlockId = table.Column<int>(type: "integer", nullable: false),
                    Depth = table.Column<float>(type: "real", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hole", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DrillBlockPoint",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Sequence = table.Column<int>(type: "integer", nullable: false),
                    X = table.Column<float>(type: "real", nullable: false),
                    Y = table.Column<float>(type: "real", nullable: false),
                    Z = table.Column<float>(type: "real", nullable: false),
                    DrillBlockId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DrillBlockPoint", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DrillBlockPoint_DrillBlock_DrillBlockId",
                        column: x => x.DrillBlockId,
                        principalTable: "DrillBlock",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HolePoint",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    X = table.Column<float>(type: "real", nullable: false),
                    Y = table.Column<float>(type: "real", nullable: false),
                    Z = table.Column<float>(type: "real", nullable: false),
                    HoleId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HolePoint", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HolePoint_Hole_HoleId",
                        column: x => x.HoleId,
                        principalTable: "Hole",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "DrillBlock",
                columns: new[] { "Id", "Name", "UpdateDate" },
                values: new object[] { 1, "DrillBlockName", new DateTime(2023, 10, 8, 5, 54, 1, 653, DateTimeKind.Utc).AddTicks(681) });

            migrationBuilder.InsertData(
                table: "Hole",
                columns: new[] { "Id", "Depth", "DrillBlockId", "Name" },
                values: new object[] { 1, 0f, 1, "HoleName" });

            migrationBuilder.InsertData(
                table: "DrillBlockPoint",
                columns: new[] { "Id", "DrillBlockId", "Sequence", "X", "Y", "Z" },
                values: new object[,]
                {
                    { 1, 1, 1, 1.4714385f, 1.4714385f, 0f },
                    { 2, 1, 2, 2.7104006f, 2.7104006f, 0f },
                    { 3, 1, 3, 3.9792213f, 3.9792213f, 0f }
                });

            migrationBuilder.InsertData(
                table: "HolePoint",
                columns: new[] { "Id", "HoleId", "X", "Y", "Z" },
                values: new object[,]
                {
                    { 1, 1, 2.942877f, 2.942877f, 0f },
                    { 2, 1, 5.420801f, 5.420801f, 0f },
                    { 3, 1, 7.9584427f, 7.9584427f, 0f }
                });

            migrationBuilder.CreateIndex(
                name: "IX_DrillBlockPoint_DrillBlockId",
                table: "DrillBlockPoint",
                column: "DrillBlockId");

            migrationBuilder.CreateIndex(
                name: "IX_HolePoint_HoleId",
                table: "HolePoint",
                column: "HoleId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DrillBlockPoint");

            migrationBuilder.DropTable(
                name: "HolePoint");

            migrationBuilder.DropTable(
                name: "DrillBlock");

            migrationBuilder.DropTable(
                name: "Hole");
        }
    }
}
