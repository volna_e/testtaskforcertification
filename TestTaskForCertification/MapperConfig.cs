﻿using AutoMapper;
using TestTaskForCertification.DTO;
using TestTaskForCertification.Entity;

namespace TestTaskForCertification
{
    public class MapperConfig
    {
        public static Mapper InitializeAutoMapper()
        {
            return new Mapper(new MapperConfiguration(config =>
            {
                config.CreateMap<DrillBlock, DrillBlockDto>().ReverseMap();
                config.CreateMap<DrillBlockPoint, DrillBlockPointDto>().ReverseMap();
                config.CreateMap<HolePoint, HolePointDto>().ReverseMap();
                config.CreateMap<Hole, HoleDto>().ReverseMap();
            }));
        }
    }
}
