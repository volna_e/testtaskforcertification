[Setup]
AppName=My Program
AppVersion=1.5
DefaultDirName={pf}\TestTaskForCertification
UsePreviousAppDir=no
[Files]
Source: "TestTaskForCertification.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "appsettings.json"; DestDir: "{app}"; Flags: ignoreversion
[Types]
Name: "Service"; Description: "Windows Service installation"
Name: "ConsoleApplication"; Description: "Console Application installation"
[Components]
Name: "Service"; Description: "Windows Service installation"; Types: Service
Name: "ConsoleApplication"; Description: "Console Application installation"; Types: ConsoleApplication
[RUN]
Filename: {sys}\sc.exe; Parameters: "create TestTaskForCertificationService start= auto binPath= ""{app}\TestTaskForCertification.exe""" ; Components: Service
Filename: {sys}\sc.exe; Parameters: "start TestTaskForCertificationService" ; Components: Service
Filename: {app}\TestTaskForCertification.exe; Components: ConsoleApplication