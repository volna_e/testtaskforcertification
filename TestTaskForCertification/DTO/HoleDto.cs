﻿namespace TestTaskForCertification.DTO
{
    public class HoleDto
    {
        public int Id { get; private set; }
        public string Name { get; set; }
        public int DrillBlockId { get; set; }
        public float Depth { get; set; }

        public HoleDto(int id, string name, float depth)
        {
            Id = id;
            Name = name;
            Depth = depth;
        }
    }
}
