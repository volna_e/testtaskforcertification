﻿namespace TestTaskForCertification.DTO
{
    public class DrillBlockPointDto
    {
        public int Id { get; private set; }
        public int Sequence { get; set; }
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
        public int DrillBlockId { get; set; }

        public DrillBlockPointDto (int id, int sequence, float x, float y, float z, int drillBlockId)
        {
            Id = id;
            Sequence = sequence;
            X = x;
            Y = y;
            Z = z;
            DrillBlockId = drillBlockId;
        }
    }
}
