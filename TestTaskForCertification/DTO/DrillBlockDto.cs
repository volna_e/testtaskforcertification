﻿namespace TestTaskForCertification.DTO
{
    public class DrillBlockDto
    {
        public int Id { get; private set; }
        public string Name { get; set; }
        public DrillBlockDto (string name, int id)
        {
            Id = id;
            Name = name;
        }
    }
}
