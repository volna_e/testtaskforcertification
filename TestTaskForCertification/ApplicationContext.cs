﻿using Microsoft.EntityFrameworkCore;
using TestTaskForCertification.Entity;

namespace TestTaskForCertification
{
    public class ApplicationContext : DbContext
    {
        protected readonly IConfiguration Configuration;

        public ApplicationContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseNpgsql(Configuration.GetConnectionString("WebApiDatabase"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            InitialDefaultValues(modelBuilder);
        }

        private void InitialDefaultValues(ModelBuilder modelBuilder)
        {
            var firstId = 1;
            var pointsCount = 3;
            var drillBlock = new DrillBlock(firstId, "DrillBlockName");
            modelBuilder.Entity<DrillBlock>().ToTable("DrillBlock").HasData(drillBlock);
            var hole = new Hole(firstId, "HoleName", default) { DrillBlockId = drillBlock.Id };
            modelBuilder.Entity<Hole>().ToTable("Hole").HasData(hole);
            Random rand = new Random();
            for (var id = firstId; id <= pointsCount; id++)
            {
                var defaultCoords = (float) rand.NextDouble() + id;
                modelBuilder.Entity<DrillBlockPoint>().ToTable("DrillBlockPoint").HasData(new DrillBlockPoint(id, id, defaultCoords, defaultCoords, default, drillBlock.Id));
                defaultCoords = 2 * defaultCoords;
                modelBuilder.Entity<HolePoint>().ToTable("HolePoint").HasData(new HolePoint(id, defaultCoords, defaultCoords, default, hole.Id));
            }
        }
    }
}
