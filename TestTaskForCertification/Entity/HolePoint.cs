﻿namespace TestTaskForCertification.Entity
{
    public class HolePoint
    {
        public int Id { get; private set; }
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
        public int HoleId { get; set; }

        public HolePoint(int id, float x, float y, float z, int holeId)
        {
            Id = id;
            X = x;
            Y = y;
            Z = z;
            HoleId = holeId;
        }
    }
}
