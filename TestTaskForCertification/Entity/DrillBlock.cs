﻿namespace TestTaskForCertification.Entity
{
    public class DrillBlock
    {
        public int Id { get; private set; }
        public string Name { get; set; }
        public DateTime UpdateDate { get; set; } = DateTime.UtcNow;
        
        public List<DrillBlockPoint>? DrillBlockPoints { get; set; }

        public DrillBlock(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
