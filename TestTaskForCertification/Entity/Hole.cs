﻿namespace TestTaskForCertification.Entity
{
    public class Hole
    {
        public int Id { get; private set; }
        public string Name { get; set; }
        public int DrillBlockId { get; set; }
        public float Depth { get; set; }
        public List<HolePoint>? holePoints { get; set; }

        public Hole(int id, string name, float depth) 
        {
            Id = id;
            Name = name;
            Depth = depth;
        }
    }
}
