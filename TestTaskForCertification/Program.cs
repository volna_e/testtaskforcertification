using Microsoft.EntityFrameworkCore;
using NLog;
using NLog.Extensions.Logging;
using NLog.Web;
using TestTaskForCertification;

var builder = WebApplication.CreateBuilder(args);
LogManager.Configuration = new NLogLoggingConfiguration(builder.Configuration.GetSection("NLog"));
var logger = LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
builder.Logging.ClearProviders();
builder.Host.UseNLog();
builder.Services.AddWindowsService(options =>
{
    options.ServiceName = "TestTaskForCertificationService";
});
builder.Services.AddSignalR();
builder.Services.AddControllers();
builder.Services.AddSingleton(typeof(IGenericRepository<>), typeof(EFGenericRepository<>)).AddSingleton<DbContext, ApplicationContext>();
builder.Services.AddSingleton(MapperConfig.InitializeAutoMapper());
var app = builder.Build();
app.UseMiddleware<RequestLoggingMiddleware>();
app.MapHub<ChatHub>("/chat");
app.MapControllers();
app.Run();
