﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using TestTaskForCertification.DTO;
using TestTaskForCertification.Entity;

namespace TestTaskForCertification.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HolePointsController : Controller
    {
        private IGenericRepository<HolePoint> holePointsRepo;
        private AutoMapper.Mapper mapper;
        private IHubContext<ChatHub> hubContext;
        private const string messageNotFound = "HolePoint Not Found";

        public HolePointsController(IGenericRepository<HolePoint> holePointsRepo, IHubContext<ChatHub> hubContext, AutoMapper.Mapper mapper)
        {
            this.holePointsRepo = holePointsRepo;
            this.mapper = mapper;
            this.hubContext = hubContext;
        }

        [HttpGet]
        public JsonResult Get()
        {
            return Json(mapper.Map<IEnumerable<HolePoint>, List<HolePointDto>>(holePointsRepo.Get()));
        }

        [HttpGet("{id}")]
        public ObjectResult Get(int id)
        {
            var holePoints = holePointsRepo.FindById(id);
            if (holePoints == null)
            {
                return NotFound(new { message = messageNotFound });
            }
            else
            {
                return new ObjectResult(mapper.Map<HolePoint, HolePointDto>(holePoints));
            }
        }

        [HttpPost]
        public async Task<ObjectResult> Post(HolePointDto holePointsDto)
        {
            await hubContext.Clients.All.SendAsync("Create", holePointsDto);
            holePointsRepo.Create(mapper.Map<HolePointDto, HolePoint>(holePointsDto));
            return new ObjectResult(holePointsDto);
        }

        [HttpPut]
        public async Task<ObjectResult> Put(HolePointDto holePointsDto)
        {
            await hubContext.Clients.All.SendAsync("Update", holePointsDto);
            holePointsRepo.Update(mapper.Map<HolePointDto, HolePoint>(holePointsDto));
            return new ObjectResult(holePointsDto);
        }

        [HttpDelete("{id}")]
        public async Task<ObjectResult> Delete(int id)
        {
            var holePoints = holePointsRepo.FindById(id);
            if (holePoints == null)
            {
                return await Task.FromResult(NotFound(new { message = messageNotFound }));
            }
            holePointsRepo.Remove(holePoints);
            var holePointsDto = mapper.Map<HolePoint, HolePointDto>(holePoints);
            await hubContext.Clients.All.SendAsync("Delete", holePointsDto);
            return new ObjectResult(holePointsDto);
        }
    }
}
