﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using TestTaskForCertification.DTO;
using TestTaskForCertification.Entity;

namespace TestTaskForCertification.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DrillBlocksController : Controller
    {
        private IGenericRepository<DrillBlock> drillBlockRepo;
        private AutoMapper.Mapper mapper;
        private IHubContext<ChatHub> hubContext;
        private const string messageNotFound = "DrillBlock Not Found";

        public DrillBlocksController(IGenericRepository<DrillBlock> drillBlockRepo, IHubContext<ChatHub> hubContext, AutoMapper.Mapper mapper)
        {
            this.drillBlockRepo = drillBlockRepo;
            this.mapper = mapper;
            this.hubContext = hubContext;
        }

        [HttpGet]
        public JsonResult Get()
        {
            return Json(mapper.Map<IEnumerable<DrillBlock>, List<DrillBlockDto>>(drillBlockRepo.Get()));
        }

        [HttpGet("{id}")]
        public ObjectResult Get(int id)
        {
            var drillBlock = drillBlockRepo.FindById(id);
            if (drillBlock == null)
            {
                return NotFound(new { message = messageNotFound });
            }
            else
            {
                return new ObjectResult(mapper.Map<DrillBlock, DrillBlockDto>(drillBlock));
            }
        }

        [HttpPost]
        public async Task<ObjectResult> Post(DrillBlockDto drillBlockDto)
        {
            await hubContext.Clients.All.SendAsync("Create", drillBlockDto);
            drillBlockRepo.Create(mapper.Map<DrillBlockDto, DrillBlock>(drillBlockDto));
            return new ObjectResult(drillBlockDto);
        }

        [HttpPut]
        public async Task<ObjectResult> Put(DrillBlockDto drillBlockDto)
        {
            await hubContext.Clients.All.SendAsync("Update", drillBlockDto);
            drillBlockRepo.Update(mapper.Map<DrillBlockDto, DrillBlock>(drillBlockDto));
            return new ObjectResult(drillBlockDto);
        }

        [HttpDelete("{id}")]
        public async Task<ObjectResult> Delete(int id)
        {
            var drillBlock = drillBlockRepo.FindById(id);
            if (drillBlock == null)
            {
                return await Task.FromResult(NotFound(new { message = messageNotFound }));
            }
            drillBlockRepo.Remove(drillBlock);
            var drillBlockDto = mapper.Map<DrillBlock, DrillBlockDto>(drillBlock);
            await hubContext.Clients.All.SendAsync("Delete", drillBlockDto);
            return new ObjectResult(drillBlockDto);
        }
    }
}
