﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using TestTaskForCertification.DTO;
using TestTaskForCertification.Entity;

namespace TestTaskForCertification.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HolesController : Controller
    {
        private IGenericRepository<Hole> holeRepo;
        private AutoMapper.Mapper mapper;
        private IHubContext<ChatHub> hubContext;
        private const string messageNotFound = "Hole Not Found";

        public HolesController(IGenericRepository<Hole> holeRepo, IHubContext<ChatHub> hubContext, AutoMapper.Mapper mapper)
        {
            this.holeRepo = holeRepo;
            this.mapper = mapper;
            this.hubContext = hubContext;
        }

        [HttpGet]
        public JsonResult Get()
        {
            return Json(mapper.Map<IEnumerable<Hole>, List<HoleDto>>(holeRepo.Get()));
        }

        [HttpGet("{id}")]
        public ObjectResult Get(int id)
        {
            var hole = holeRepo.FindById(id);
            if (hole == null)
            {
                return NotFound(new { message = messageNotFound });
            }
            else
            {
                return new ObjectResult(mapper.Map<Hole, HoleDto>(hole));
            }
        }

        [HttpPost]
        public async Task<ObjectResult> Post(HoleDto holeDto)
        {
            await hubContext.Clients.All.SendAsync("Create", holeDto);
            holeRepo.Create(mapper.Map<HoleDto, Hole>(holeDto));
            return new ObjectResult(holeDto);
        }

        [HttpPut]
        public async Task<ObjectResult> Put(HoleDto holeDto)
        {
            await hubContext.Clients.All.SendAsync("Update", holeDto);
            holeRepo.Update(mapper.Map<HoleDto, Hole>(holeDto));
            return new ObjectResult(holeDto);
        }

        [HttpDelete("{id}")]
        public async Task<ObjectResult> Delete(int id)
        {
            var hole = holeRepo.FindById(id);
            if (hole == null)
            {
                return await Task.FromResult(NotFound(new { message = messageNotFound }));
            }
            holeRepo.Remove(hole);
            var holeDto = mapper.Map<Hole, HoleDto>(hole);
            await hubContext.Clients.All.SendAsync("Delete", holeDto);
            return new ObjectResult(holeDto);
        }
    }
}
