﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using TestTaskForCertification.DTO;
using TestTaskForCertification.Entity;

namespace TestTaskForCertification.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DrillBlockPointsController : Controller
    {
        private IGenericRepository<DrillBlockPoint> drillBlockPointsRepo;
        private AutoMapper.Mapper mapper;
        private IHubContext<ChatHub> hubContext;
        private const string messageNotFound = "DrillBlockPoint Not Found";

        public DrillBlockPointsController(IGenericRepository<DrillBlockPoint> drillBlockPointsRepo, IHubContext<ChatHub> hubContext, AutoMapper.Mapper mapper)
        {
            this.drillBlockPointsRepo = drillBlockPointsRepo;
            this.mapper = mapper;
            this.hubContext = hubContext;
        }

        [HttpGet]
        public JsonResult Get()
        {
            return Json(mapper.Map<IEnumerable<DrillBlockPoint>, List<DrillBlockPointDto>>(drillBlockPointsRepo.Get()));
        }

        [HttpGet("{id}")]
        public ObjectResult Get(int id)
        {
            var drillBlockPoints = drillBlockPointsRepo.FindById(id);
            if (drillBlockPoints == null)
            {
                return NotFound(new { message = messageNotFound });
            }
            else
            {
                return new ObjectResult(mapper.Map<DrillBlockPoint, DrillBlockPointDto>(drillBlockPoints));
            }
        }

        [HttpPost]
        public async Task<ObjectResult> Post(DrillBlockPointDto drillBlockPointsDto)
        {
            await hubContext.Clients.All.SendAsync("Create", drillBlockPointsDto);
            drillBlockPointsRepo.Create(mapper.Map<DrillBlockPointDto, DrillBlockPoint>(drillBlockPointsDto));
            return new ObjectResult(drillBlockPointsDto);
        }

        [HttpPut]
        public async Task<ObjectResult> Put(DrillBlockPointDto drillBlockPointsDto)
        {
            await hubContext.Clients.All.SendAsync("Update", drillBlockPointsDto);
            drillBlockPointsRepo.Update(mapper.Map<DrillBlockPointDto, DrillBlockPoint>(drillBlockPointsDto));
            return new ObjectResult(drillBlockPointsDto);
        }

        [HttpDelete("{id}")]
        public async Task<ObjectResult> Delete(int id)
        {
            var drillBlockPoints = drillBlockPointsRepo.FindById(id);
            if (drillBlockPoints == null)
            {
                return await Task.FromResult(NotFound(new { message = messageNotFound }));
            }
            drillBlockPointsRepo.Remove(drillBlockPoints);
            var drillBlockPointsDto = mapper.Map<DrillBlockPoint, DrillBlockPointDto>(drillBlockPoints);
            await hubContext.Clients.All.SendAsync("Delete", drillBlockPointsDto);
            return new ObjectResult(drillBlockPointsDto);
        }
    }
}
