﻿namespace TestTaskForCertification
{
    public class RequestLoggingMiddleware
    {
        private readonly RequestDelegate next;
        private readonly ILogger logger;
        public RequestLoggingMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            this.next = next;
            logger = loggerFactory.CreateLogger<RequestLoggingMiddleware>();
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var url = context.Request.Scheme.ToString() + context.Request.Path.ToString() + context.Request.QueryString.ToString();
            logger.LogInformation(url);
            await next.Invoke(context);
        }
    }
}
